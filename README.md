sign here
==========

take a look at the Project description on the [Schauspiel Essen](https://www.theater-essen.de/spielplan/a-z/sign-here/) website or at [machinaex.com](https://www.machinaex.com/de/projects).

When developing **Sign Here** we created some devices, props and software we really like and enjoyed building. We try to publish what we can but it is very likely that this repository lacks a lot of major and minor knowledge.

If you are interested in finding out more about how we built things, don't hesitate to contact us.



Informations about the 2020 Game Theatre Project at Schauspiel Essen.